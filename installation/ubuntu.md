# Ubuntu

## Intro

To get started with **Ubuntu**, obviously you'll need to install it first. You can follow the advice below to achieve this.

**Info:** *You could likely also do this using [Linux on WSL](https://docs.microsoft.com/en-us/windows/wsl/install), however this option does lack certain features.*

**Note:** *I am using **Ubuntu 22.04 LTS (`Ubuntu Jammy`)** in this guide.*

<details>

<summary>Install advice. Click to expand</summary>

***

Start by downloading **[Ubuntu Desktop](https://ubuntu.com/download/desktop)**.

**Info:** *`LTS` stands for `long term support` so ideally you should try and use an **Ubuntu `LTS`** version where possible.*

To install on **bare-metal** you'll need to create a *bootable `USB` drive* if not using a TFTP server. You can do this with various tools such as **[Rufus](https://rufus.ie/en/)** or **[Universal USB Installer](https://www.pendrivelinux.com/universal-usb-installer-easy-as-1-2-3/)**.

**Note:** To install on a **virtual machine**, you may need to upload the **Ubuntu Desktop ISO** to your `Hypervisor`, or have a file mount configured to be able to mount the `ISO` for use with the *VM*.

Once ready, set the machine to boot from the **Ubuntu Desktop ISO**.

The **Ubuntu** install process is pretty straightforward, but you can watch the short video below for help if needed.

**Info:** *I **always** pick minimal installation during the setup process! This saves time with the installation and with updates after installation, as well as not installing a lot of the shit we **don't need** or **won't use**.*

[![Ubuntu Install](./../images/ubuntu-video-image.png)](https://youtu.be/n0QLxQ2iv24)

Once you have **Ubuntu** installed, the first thing you should always do is **update** and **upgrade** the currently `installed packages`. *(Failing to at least run `apt-update` can, and likely will cause you problems later!)*

<details>

<summary>I'll create a Linux command line guide at some point, but here are a few key commands</summary>

- **`cd`**
  - Change directory
    - **Note:** You can use `cd ..` to navigate back one folder. You can also use `cd` followed by a space then press enter to navigate straight back to your home directory.
- **`pwd`**
  - Print working directory (Shows which folder you are currently in)
- **`sudo`**
  - Run commands with admin rights
- **`&&`**
  - You can run multiple commands at once by combining them with `&&`
- **`mkdir`**
  - Create a directory
- **`rm`**
  - Remove a file or directory

</details>

Start by opening the **Terminal** either by finding the app shortcut or by pressing `CTRL+SHIFT+T` whilst on the **Desktop**, and then run the following command:

`sudo apt-get update && sudo apt-get full-upgrade -y`

![Update command](./../images/sudo-update.png)

Broken down this means;

- **`sudo apt-get update`**
  - *Update the currently installed repositories and packages information with root privileges*
- **`&&`**
  - *and also do;*
- **`sudo apt-get full-upgrade -y`**
  - *Install available updates as `root` **without** confirmation*

The `-y` switch prevents you from being asked if you want to confirm the install. **(You should really only use this switch if you trust what is being installed!)**

**Info:** *You'll be prompted to enter your password to run `sudo` commands; Neither the password or any symbols will appear when being entered.*

The `full-upgrade` command is the *recommended* way of updating certain `rolling distros`, such as **Kali Linux**. Try and use this command **instead** of `apt-upgrade` *where possible*.

</details>

***

## Requirements

You'll first need to install the **Docker** `dependencies` and add the **Docker** `repository` to your machine using the below commands, to install Docker using `apt`.

**Note:** *See [here](https://docs.docker.com/engine/install/ubuntu/) for more information.*

### Install Dependencies

```bash
sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

### Add Repo GPG Key

```bash
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
```

### Add Repository

```bash
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

***

## Install Docker

```bash
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```

You can test the install was successfull by using the following command:

```bash
sudo docker run hello-world
```

***

## Next Steps

If you want Docker to run when the machine boots, you can enable the system service with this command:

```bash
sudo systemctl enable docker
```

Alternatively if you do not want Docker to run when the machine boots, you can disable the system service with this command:

```bash
sudo systemctl disable docker
```

To start, stop and restart the Docker service, you can use these commands:

```bash
sudo systemctl docker start
sudo systemctl docker stop
sudo systemctl restart docker
```

Additionally you may want to add your user to the Docker system group, to avoid permission errors later and to be able to run Docker commands without the sudo prefix. Use the command below to achieve this, replacing `doris` with your username.

```bash
sudo usermod -aG docker doris
```

<details>

<summary>To use Docker, you'll need to know some basic commands!</summary>

- `docker run <containerName>`
  - *Replacing `<containerName>` with the name of the container you want to run*
- `docker stop <containerName>`
  - *Replacing `<containerName>` with the name of the container you want to stop*
- `docker rm <containerName>`
  - *Replacing `<containerName>` with the name of the container you want to remove*
- `docker logs <containerName>`
  - *Replacing `<containerName>` with the name of the container you want to view the logs for*
- `docker ps -a`
  - *Show all containers*
- `docker exec -it <containerName> /bin/bash`
  - *Replacing `<containerName>` with the name of the container you want to connect to via command line **(Replace `/bin/bash` with which ever shell is needed or available)***

</details>

***

## Finish

And that's it, you are done! 👏

We could have simplified the **Docker** install process by using the `convenience script` method shown below. However, doing it this way gives you a feel for how to install and manage packages in **Ubuntu**, as well as managing `system services` from the **Terminal**.

```bash
curl -fsSL https://get.docker.com -o get-docker.sh
sudo sh get-docker.sh
```

Now that you have **Docker** installed and running it's time to move on to the fun stuff! Follow along with the *[Media Server](https://gitlab.com/N3UR0515/NeuroVision/-/blob/main/README.md) guide* to start your first **Docker** project!
