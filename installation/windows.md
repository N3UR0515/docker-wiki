# Windows

## Intro

Installing **Docker** on **Windows 10/11** is relatively straightforward, thanks to the **[Docker Desktop Installer](https://docs.docker.com/desktop/install/windows-install/)**.

**Info:** *The **Docker Desktop Installer** installs **Docker Engine** and **Docker CLI** with the **Docker Compose** plugin.*

## Requirements

We will first want to make sure **Windows Subsystem for Linux ([WSL2](https://docs.microsoft.com/en-us/windows/wsl/install))** is installed and running ***before*** installing **Docker Desktop**. This can be done by running the below command in *either* **PowerShell** or **Windows Command Prompt** as an **Administrator**, and then **rebooting** your machine.

```powershell
wsl --install
```

This command will enable the features necessary to run **WSL** and also install the **Ubuntu** distribution of Linux. To change the default **Linux** `distribution` that is installed, you can run the same command with the `-d` switch *(Examples shown below)*.

- **Kali Linux**
  - `wsl --install -d kali-linux`

- **Debian**
  - `wsl --install -d Debian`

- **Ubuntu-18.04**
  - `wsl --install -d Ubuntu-18.04`

Once `WSL2` has finished installing, you need to create the user account for the Linux distribution you installed to use.
To do this, open the Linux distribution you installed previously from the `Start Menu` ![Start Button](./../images/start-button.png).

*If you installed the default `Ubuntu`, look for Ubuntu etc*

When prompted by the `terminal`, enter a username and password.

**Note:** *Neither the password or any symbols will appear when being entered!*.

## Installation

Next, install **Docker Desktop** by downloading and running the **[Docker Desktop Installer](https://desktop.docker.com/win/main/amd64/Docker%20Desktop%20Installer.exe)** as an **Administrator**.

Once the installation has finished successfully, simply open **Docker Desktop** from the `Start Menu` ![Start Button](./../images/start-button.png), and you are good to go! 👏
