---
description: >-
  Welcome to my guide! Here you'll find information about how to install
  and use Docker, along with some useful examples.
cover: .gitbook/assets/docker-desktop-bg.jpg
coverY: 0
toc: yes
---

# Docker
