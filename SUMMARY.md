# Table of contents

* [Docker](README.md)

## Installation

* [Ubuntu](installation/ubuntu.md)
* [Windows](installation/windows.md)

## Secrets

* [.env](secrets/.env.md)

## Deployment

* [Command Line](deployment/command-line.md)
* [Dockerfile](deployment/dockerfile.md)

## Examples

* [Links](examples/links.md)
